Blockpraktikum Machine Learning mit empirischen Daten
=====================================================

Repository zum gemeinsamen arbeiten für Gruppe 7
------------------------------------------------

Teilnehmer:
- ~~Verena Bender~~
- Daniel Schubert

Thema Tag 2 / Paperrunde:
- Machine Learning for Microbial identification and atimicrobial susceptibility testing on MALDI-TOF mass spectra

Praktikumsaufgabe:
- 4. Clustering
